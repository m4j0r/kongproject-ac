yum -y install nginx

service nginx start #启动 nginx 服务

service nginx stop #停止 nginx 服务

service nginx restart #重启 nginx 服务

vim /etc/nginx/nginx.conf


location / {
    proxy_pass http://localhost:5001;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    client_max_body_size 5M;
}

Qcloud申请免费的ssl证书
只支持fatecore.com域名，x.fatecore.com不支持。
https://console.cloud.tencent.com/ssl/detail/SCBHnyX0
https://juejin.im/post/5b83c569f265da436152f7d5

用默认配置里面的ssl配置，填crt/key的位置，加location即可。
80/443同时开启。