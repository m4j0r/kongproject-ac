# -*- coding: UTF-8 -*-

"""
app_config.py will be storing all the module configs.
Here the db uses mysql.
"""

import os
_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False

ADMINS = frozenset(['your_email_here@email.com'])
SECRET_KEY = 'yaojunrong123'
SQLALCHEMY_DATABASE_URI = 'mysql://root:OceanLily1124_&asd@172.27.0.6:11230/kong?charset=utf8mb4'
# SQLALCHEMY_DATABASE_URI = 'mysql://root:OceanLily1124_&asd@cdb-oq8rdhmo.cd.tencentcdb.com:10028/kong?charset=utf8mb4'
# SQLALCHEMY_DATABASE_URI = 'mysql://root:@127.0.0.1:3306/kong?charset=utf8mb4'

DATABASE_CONNECT_OPTIONS = {}

CSRF_ENABLED = True
CSRF_SESSION_KEY = "123"

# Customize and add the blow if you'd like to use recaptcha. SSL is enabled
# by default and this is recaptcha v2: tap "I'm not a robot" checkbox instead
# of answering a riddle.
# Please see: https://www.google.com/recaptcha
RECAPTCHA_DATA_ATTRS = {'theme': 'light'}
RECAPTCHA_PUBLIC_KEY = '6LcGJzkUAAAAAAumiO7yfDEDHMkyomZVSe_fraYe'
RECAPTCHA_PRIVATE_KEY = '6LcGJzkUAAAAAGDQ-_DXdBDuHPs48GnySHfiM9Ol'

BRAND = u"FateCore Studio" #??? 3.x不都是UnicodeLength吗，请和#1的project一致
BRAND_DESC = u"Fate's private projects hosting space!" #作为默认的meta description
BRAND_ICON = "http://fate2.oss-cn-shanghai.aliyuncs.com/kong/kong_icon.png"   #作为默认的icon
DOMAIN = "fatecore.com"
ROOT_URL = "" #这里到线上要给出真的域名 直接放开，自动用当时的根域名

STATIC_ROOT = "./kong/static/"
STATIC_URL = ROOT_URL + "/static/"
UPLOAD_FOLDER = _basedir