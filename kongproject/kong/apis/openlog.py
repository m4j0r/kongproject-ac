# -*- coding: utf-8 -*-
"""
"""
from kong import db

class OpenLog(db.Model):
    """
    """
    __tablename__ = 'openlog'
    id = db.Column(db.Integer, primary_key=True)
    pname = db.Column(db.String, default="default-project") #项目分别
    tag = db.Column(db.String, default="default-tag") #项目内tag
    agent = db.Column(db.String, default="default-agent") #指明用户对象
    text = db.Column(db.String, default="default-text") #log内容
    created_on = db.Column(db.DateTime, default=db.func.now())

    def get_title(self):
        return self.text.split(',')[0]

    def get_url(self):
        return self.text.split(',')[1]

    def __init__(self, pname, tag, agent, text):
        self.pname = pname
        self.tag = tag
        self.agent = agent
        self.text = text

    def __repr__(self):
        return '<OpenLog %r>' % (self.pname)

