# -*- coding: utf-8 -*-
"""
All view code for async get/post calls towards the server
must be contained in this file.
"""
from flask import (Blueprint, request, render_template, flash, g,
        session, redirect, url_for, jsonify, abort)
from werkzeug import check_password_hash, generate_password_hash

from kong import db
from kong.users.models import User
from kong.threads.models import Thread, Comment
from kong.users.decorators import requires_login
from kong.apis.openlog import OpenLog
import base64
import random
import json

mod = Blueprint('apis', __name__, url_prefix='/apis')

@mod.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])

@mod.route('/comments/submit/', methods=['POST'])
@requires_login
def submit_comment():
    """
    Submit comments via ajax
    """
    thread_id = int(request.form['thread_id'])
    comment_text = request.form['comment_text']
    comment_parent_id = request.form['parent_id'] # empty means none

    if not comment_text:
        abort(404)

    thread = Thread.query.get_or_404(int(thread_id))
    comment = thread.add_comment(comment_text, comment_parent_id,
            g.user.id)

    return jsonify(comment_text=comment.text, date=comment.pretty_date(),
            username=g.user.username, comment_id=comment.id,
            margin_left=comment.get_margin_left())

@mod.route('/threads/vote/', methods=['POST'])
def vote_thread():
    """
    Submit votes via ajax
    """
    thread_id = int(request.form['thread_id'])

    user_id = None
    if g.user:
        user_id = g.user.id

    if not thread_id:
        abort(404)

    thread = Thread.query.get_or_404(int(thread_id))
    vote_status = thread.vote(user_id=user_id)
    return jsonify(new_votes=thread.votes, vote_status=vote_status)

@mod.route('/comments/vote/', methods=['POST'])
@requires_login
def vote_comment():
    """
    Submit votes via ajax
    """
    comment_id = int(request.form['comment_id'])
    user_id = g.user.id

    if not comment_id:
        abort(404)

    comment = Comment.query.get_or_404(int(comment_id))
    comment.vote(user_id=user_id)
    return jsonify(new_votes=comment.get_votes())

@mod.route('/threads/toggle_pin/', methods=['POST'])
def toggle_pin():
    if not g.user.is_admin():
        return jsonify(dict(error=2, msg='no permission'))

    thread_id = int(request.form['thread_id'])
    print('toggle', thread_id)
    thread = Thread.query.filter_by(id=thread_id).first()
    if thread:
        thread.pin = 1 if thread.pin == 0 else 0
        db.session.commit()
        return jsonify(dict(error=0,msg='ok'))
    else:
        return jsonify(dict(error=1, msg="thread error"))


@mod.route('/threads/set_status/', methods=['POST'])
def set_status():
    if not g.user.is_admin():
        return jsonify(dict(error=2, msg='no permission'))

    thread_id = int(request.form['thread_id'])
    new_status = int(request.form['status'])
    thread = Thread.query.filter_by(id=thread_id).first()
    if thread:
        thread.status = new_status
        db.session.commit()
        return jsonify(dict(error=0,msg='ok'))
    else:
        return jsonify(dict(error=1, msg="thread error"))

@mod.route('/openlog/', methods=['POST'])
def open_log():
    try:
        if request.form['s'] == 'fccp':
            data = request.form['d']
            if data:
                log_text = base64.b64decode(data)
                log_text = json.loads(log_text)
                open_log = OpenLog(log_text['pname'], log_text['tag'], log_text['agent'], log_text['text'])
                if log_text['agent'] not in ['O2MB01A0752', 'O2M115F53F4', 'O2MD50A6A4C', 'O2MB01A0752', 'O2M90C73B86-V4.1.0']:#有几个大量报错的，剔除掉
                    db.session.add(open_log)
                    db.session.commit()
    except Exception:
        pass
    
    fakemsg = ['hello world', 'server rejected', 'err miniser down', 'ok', 'ok']
    return jsonify(dict(error=random.randint(0, 5),msg=random.choice(fakemsg)))
