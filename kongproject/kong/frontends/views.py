# -*- coding: utf-8 -*-
"""
"""
from flask import (Blueprint, request, render_template, flash,
    g, session, redirect, url_for)
from werkzeug import check_password_hash, generate_password_hash

from kong import db
from kong import search as search_module # don't override function name
from kong.users.forms import RegisterForm, LoginForm
from kong.users.models import User
from kong.threads.models import Thread
from kong.subreddits.models import Subreddit
from kong.users.decorators import requires_login
from kong.security.sec_util import secode_validate

mod = Blueprint('frontends', __name__, url_prefix='')

@mod.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])

def home_subreddit():
    return Subreddit.query.get_or_404(1)

def get_subreddits():
    """
    important and widely imported method because a list of
    the top 30 subreddits are present on every page in the sidebar
    """
    subreddits = Subreddit.query.all()[:25]
    return subreddits

def process_thread_paginator(trending=False, rs=None, subreddit=None):
    """
    abstracted because many sources pull from a thread listing
    source (subreddit permalink, homepage, etc)
    """
    threads_per_page = 15
    cur_page = request.args.get('page') or 1
    cur_page = int(cur_page)
    thread_paginator = None

    # if we are passing in a resultset, that means we are just looking to
    # quickly paginate some arbitrary data, no sorting
    if rs:
        thread_paginator = rs.paginate(cur_page, per_page=threads_per_page,
            error_out=True)
        return thread_paginator

    # sexy line of code :)
    base_query = subreddit.threads if subreddit else Thread.query
    base_query = base_query.filter(Thread.status != 0).order_by(Thread.order.desc())  #非dead

    if trending:
        #https://stackoverflow.com/questions/16966163/sqlalchemy-order-by-calculated-column
        #我们用计算列的方式来置顶。可能有性能问题
        order_column = Thread.pin * 100000 + Thread.votes
        thread_paginator = base_query.order_by(db.desc(order_column)).\
        paginate(cur_page, per_page=threads_per_page, error_out=True)
    else:
        order_column = Thread.pin * 100000 + Thread.hotness
        thread_paginator = base_query.order_by(db.desc(order_column)).\
                paginate(cur_page, per_page=threads_per_page, error_out=True)
    return thread_paginator


#@mod.route('/<regex("trending"):trending>/')
@mod.route('/')
def home(trending=False):
    """
    If not trending we order by creation date
    """
    trending = True if request.args.get('trending') else False
    subreddits = get_subreddits()
    thread_paginator = process_thread_paginator(trending)

    return render_template('home.html', user=g.user,
            subreddits=subreddits, cur_subreddit=home_subreddit(),
            thread_paginator=thread_paginator)

@mod.route('/search/', methods=['GET'])
def search():
    """
    Allows users to search threads and comments
    """
    query = request.args.get('query')
    rs = search_module.search(query, orderby='creation', search_title=True,
            search_text=True, limit=100)

    thread_paginator = process_thread_paginator(rs=rs)
    rs = rs.all()
    num_searches = len(rs)
    subreddits = get_subreddits()

    return render_template('home.html', user=g.user,
            subreddits=subreddits, cur_subreddit=home_subreddit(),
            thread_paginator=thread_paginator, num_searches=num_searches)

@mod.route('/login/', methods=['GET', 'POST'])
def login():
    """
    We had to do some extra work to route the user back to
    his or her original place before logging in
    """
    if g.user:
        return redirect(url_for('frontends.home'))

    next = ''
    if request.method == 'GET':
        if 'next' in request.args:
            next = request.args['next']

    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and check_password_hash(user.password, form.password.data):
            session['user_id'] = user.id

            #总是前往用户中心
            # if 'next' in request.form and request.form['next']:
            #     return redirect(request.form['next'])
            return redirect(url_for('my.my_status'))


        flash(u'用户名或密码错误！', 'danger')
    return render_template("login.html", form=form, next=next)

@mod.route('/logout/', methods=['GET', 'POST'])
@requires_login
def logout():
    session.pop('user_id', None)
    return redirect(url_for('frontends.home'))

@mod.route('/register/', methods=['GET', 'POST'])
def register():
    """
    """
    next = ''
    if request.method == 'GET':
        # flash(u'请妥善保管注册资料！', 'danger')
        if 'next' in request.args:
            next = request.args['next']

    form = RegisterForm(request.form)
    if form.validate_on_submit():

        if secode_validate():

            user = User(username=form.username.data, email=form.email.data, \
                    password=generate_password_hash(form.password.data))
            db.session.add(user)
            try:
                db.session.commit()
                session['user_id'] = user.id

                flash(u'欢迎新用户！', 'success')
                # 不再关心next，始终前往用户中心
                # if 'next' in request.form and request.form['next']:
                    # return redirect(request.form['next'])
                return redirect(url_for('my.my_status'))
            except Exception:
                flash(u'用户名或email已经被占用!', 'danger')
        else:
            flash(u'验证码错误!', 'danger')

    return render_template("register.html", form=form, next=next)

