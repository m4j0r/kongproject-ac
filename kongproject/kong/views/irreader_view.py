# -*- coding: utf-8 -*-

from urllib.parse import urlparse
from flask import (Blueprint, request, render_template, flash, g,
        session, redirect, url_for, jsonify, abort)
from werkzeug import check_password_hash, generate_password_hash
from kong.apis.openlog import OpenLog

from kong import db
from kong.users.models import User
from kong.models.irreader_model import IrreaderTarget
from kong.users.decorators import requires_login, requires_admin
from kong import utils
import base64
import random
import json
import uuid
import time
from kong import app

mod = Blueprint('irreader', __name__, url_prefix='/irreader')

@mod.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])

@mod.route('/get_sources/', methods=['POST'])
def get_sources():
    """
    q = query word
    p = page number
    s = page size
    c = category
    r = rank type
    """
    page = 0
    total = 10
    category = request.form.get('c', None)
    keyword = request.form.get('q', None)
    page_size = int(request.form.get('s', 20))
    page_number = int(request.form.get('p', 0))
    rank_way = request.form.get('r', 'new')
    #注意了，非字符串的，要手动转成对应类型！

    if page_size > 30:
            page_size = 1
    if page_number < 0:
            page_number = 0

    offset = page_number*page_size
    limit = page_size

    query = IrreaderTarget.query.filter_by(admin_state=1)
    if category and category != 'latest': #new的时候，看最新，不分category
        query = query.filter_by(category=category)

    if keyword and len(keyword) > 0:
        query = query.filter(IrreaderTarget.name.contains(keyword))

    if rank_way == 'new':
        query = query.order_by(IrreaderTarget.id.desc())
    else:
        query = query.order_by(IrreaderTarget.sub_num.desc())

    query = query.offset(offset).limit(limit)

    targets = query.all()
    targets = [x.to_dict() for x in targets]
    result = dict(err=0, msg="", page=page, total=total, targets=targets)

    return jsonify(result)

@mod.route('/sub_num_plus/', methods=['POST'])
def sub_num_plus():
    target_id = request.form.get('id', None)
    if target_id:
        target_id = int(target_id)
        target = IrreaderTarget.query.filter_by(id=target_id).first()
        if target:
                target.sub_num += 1
                db.session.commit()
    
    result = dict(err=0, msg="")

    return jsonify(result)

@mod.route('/upload_source/', methods=['POST'])
def upload_sources():
    target = IrreaderTarget()
    target.sid = str(uuid.uuid4())
    target.name = request.form.get('name', '')
    target.description = request.form.get('description', '')
    target.source_url = request.form.get('sourceUrl', '')
    target.icon = request.form.get('icon', '')
    target.type = int(request.form.get('type', '0'))
    target.category = ''
    target.keywords = ''
    target.author = ''
    target.charset = request.form.get('charset', 'utf-8')
    target.min_len = 0 #不再使用了
    target.csser = request.form.get('csser', '')
    target.version = 0
    target.sub_num = 1
    target.grade = 0
    target.add_time = int(time.time()/1000)
    target.admin_state = 0
    target.admin_note = ''
    target.searchtext = target.name + target.description
    target.config = request.form.get('config')
    ignore_keywords = ['baidu.com', 'rsshub', 'kindle4rss', 'qnmlgb.tech', 'zhihu.com', 'izgq.net',
                       'gov.cn', 'cnmlgb', 'weibo', 'qnmlgb.tech', 'weapp.design', 'feedburner.com', 
                       'sciencedirect', 'feed43']
    ignore = False
    for kw in ignore_keywords:
        if kw in target.source_url:
            ignore = True
            app.logger.info('user uploaded source ignored=%s', target.source_url)
            break
    if target.type == 0 and target.csser == '':
        app.logger.info('web source but no csser, ignored=%s', target.source_url)
        ignore = True

    if len(target.source_url) > 60:
        app.logger.info("source to long, ignored=%s", target.source_url)
        ignore = True

    if not ignore:
        db.session.add(target)
        try:
            db.session.commit()
        except Exception as e:
            # source url dulp
            db.session.rollback()
    
    result = dict(err=0, msg="")
    return jsonify(result)

@mod.route('/target-search/', methods=['get'])
def target_search():
    """
    源市场界面
    """
    return render_template('irreader/target-search.html')



@mod.route('/star-list/', methods=['get'])
@requires_admin
def star_list():
    '''
    查看用户的匿名收藏
    '''
    stars = OpenLog.query.filter_by(tag='star').order_by(OpenLog.id.desc()).limit(500).all()

    return render_template('irreader/star-list.html', stars=stars)

@mod.route('/target-manager/', methods=['get'])
@requires_login
def target_manager():
    '''
    源市场管理界面
    主要是用来审核用户分享的源市场
    '''
    if g.user.is_admin():
        #找到一个未审核的
        target = IrreaderTarget.query.filter_by(admin_state=0).first()
        if not target:
            abort(404)
        should_try_favicon = False
        app.logger.info('targer raw icon=%s', target.icon)
        if target.icon and len(target.icon) > 0:
            #如果有icon则判断该icon是否可达
            if not utils.is_image_and_ready(target.icon):
                print('try old icon, buy failed', target.icon)
                should_try_favicon = True
            else:
                print('old icon is good', target.icon)
        else:
            print('no old icon')
            should_try_favicon = True
        
        if should_try_favicon:
            #需要尝试用/favicon.ico
            ppd = urlparse(target.source_url)
            favicon_url = '{}://{}/favicon.ico'.format(ppd.scheme, ppd.netloc)
            print('try new icon', favicon_url)
            if utils.is_image_and_ready(favicon_url):
                target.icon = favicon_url
            else:
                target.icon = ""
                print('try favicon, buy failed', favicon_url)
            db.session.commit()
        return render_template('irreader/target-manager.html', target=target)
    else:
        abort(404)

@mod.route('/target-judge/', methods=['POST'])
@requires_login
def judge_source():
    '''
    审核的接口
    '''
    if g.user.is_admin():
        target_id = request.form.get('target_id', 0)
        target_id = int(target_id)
        new_state = request.form.get('state', 1) #1ok 2 reject
        if target_id > 0:
            target = IrreaderTarget.query.filter_by(id=target_id).first()
            target.admin_state = new_state
            db.session.commit()
        result = dict(err=0, msg="")
        return jsonify(result)
    else:
        abort(404)