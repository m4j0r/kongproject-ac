# -*- coding: utf-8 -*-
"""
All view code for async get/post calls towards the server
must be contained in this file.
"""
from flask import (Blueprint, request, render_template, flash, g,
        session, redirect, url_for, jsonify, abort)
from werkzeug import check_password_hash, generate_password_hash

from kong import db
from kong.models.owm_model import Owm
from kong.users.models import User
from kong.threads.models import Thread, Comment
from kong.users.decorators import requires_login
from kong.apis.openlog import OpenLog
from kong import utils
import base64
import random
import json
import uuid
import time


mod = Blueprint('owm', __name__, url_prefix='/owm')

@mod.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])

@mod.route('/get_token/', methods=['GET', 'POST'])
@requires_login
def owm_get_token():
    """
    owm时，用户登录后会调用此接口获取token。
    若user的owm未初始化，则会初始化；
    """

    owm = Owm.query.filter_by(user_id=g.user.id).first()
    
    if not owm:
        #如果不存在就初始化它
        owm = Owm()
        owm.user_id = g.user.id
        owm.state = 0
        owm.pay_end_day = 0
        owm.admin_note = ""
        owm.sync_time = 0
        owm.sync_data = ""
        owm.token = uuid.uuid1()
        db.session.add(owm)
        db.session.commit()
        owm = Owm.query.filter_by(user_id=g.user.id).first()

    return jsonify(token=owm.token)


@mod.route('/get_data/', methods=['GET', 'POST'])
def owm_get_data():
    """
    获取owm信息，
    通过token来校验；
    """
    token = request.args.get('token', default=None)
    if token:
        owm = Owm.query.filter_by(token=token).first()
        if owm:
            user = User.query.filter_by(id=owm.user_id).first()
            if user:
                return jsonify(err=0, msg="", pay_end_day=owm.pay_end_day, sync_data=owm.sync_data, username=user.username, email=user.email)

    return jsonify(err=1, msg="OWM token failed!")

@mod.route('/set_data/', methods=['post'])
def owm_set_data():
    """
    设置同步data
    """
    token = request.form.get('token', default=None)
    data = request.form.get('data', default=None)
    if token and data:
        owm = Owm.query.filter_by(token=token).first()
        if (owm):
                owm.sync_data = data
                owm.sync_time = utils.get_time()
                db.session.commit()    
                return jsonify(err=0, msg="")

    return jsonify(err=1, msg="Sync data failed!")



@mod.route('/silent/is/hell/', methods=['get'])
@requires_login
def owm_admin_page():
    """
    admin充值管理界面
    """
    if g.user.is_admin():
        return render_template('owm/admin.html')
    else:
        abort(404)


@mod.route('/get_pay_end_day/', methods=['post'])
@requires_login
def owm_get_pay_end_day():
    """
    admin获取某个用户的时间
    """
    username = request.form.get('username', default=None)
    if username and g.user.is_admin():
        user = User.query.filter_by(username=username).first()
        if user:
            owm = Owm.query.filter_by(user_id=user.id).first()
            if owm:
                return jsonify(err=0, msg="", pay_end_day=owm.pay_end_day)
            else:
                owm = Owm()
                owm.user_id = user.id
                owm.state = 0
                owm.pay_end_day = 0
                owm.admin_note = ""
                owm.sync_time = 0
                owm.sync_data = ""
                owm.token = uuid.uuid1()
                db.session.add(owm)
                db.session.commit()
                return jsonify(err=3, msg="user has not init owm token! Now we gen it!")
        else:
            return jsonify(err=2, msg="user not found!")
    return jsonify(err=1, msg="Forbidden!")

def get_today():
        return int(time.time()/(24*3600))

@mod.route('/add_pay_end_day/', methods=['post'])
@requires_login
def owm_add_pay_end_day():
    """
    admin，延长owm VIP时间
    TODO 记录log
    """
    username = request.form.get('username', default=None)
    add_day = int(request.form.get('add_day', default=0))
    if username and g.user.is_admin():
        user = User.query.filter_by(username=username).first()
        if user:
            owm = Owm.query.filter_by(user_id=user.id).first()
            if owm:
                #如果原来小于今天，则从今天开始加
                if owm.pay_end_day < get_today():
                        owm.pay_end_day = get_today()

                owm.pay_end_day += add_day
                db.session.commit()
                return jsonify(err=0, msg="")
        else:
            return jsonify(err=2, msg="user not found!")
    return jsonify(err=1, msg="Forbidden!")