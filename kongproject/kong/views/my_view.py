# -*- coding: utf-8 -*-

from flask import (Blueprint, request, render_template, flash, g,
        session, redirect, url_for, jsonify, abort)
from werkzeug import check_password_hash, generate_password_hash

from kong import db
from kong.models.owm_model import Owm
from kong.users.models import User
from kong.threads.models import Thread, Comment
from kong.users.decorators import requires_login
from kong.apis.openlog import OpenLog
from kong.frontends.views import get_subreddits
from kong import utils
from kong.forms import AlterPasswordForm
import base64
import random
import json
import uuid


mod = Blueprint('my', __name__, url_prefix='/my')

@mod.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])

@mod.route('/status/', methods=['GET', 'POST'])
@requires_login
def my_status():
    """
    我的用户中心。
    """
    owm = Owm.query.filter_by(user_id=g.user.id).first()
    data = dict(owm_left_day=0, owm_user_type="未激活")
    if owm:
        left_day = owm.pay_end_day - utils.get_today() + 1
        if left_day < 0:
            left_day = 0
        
        if left_day > (100*365):
            data['owm_left_day'] = u'永久'
        else:
            data['owm_left_day'] = left_day
            
        if left_day > 0:
            data['owm_user_type'] = u'VIP'
        else:
            data['owm_user_type'] = u'普通用户'


    return render_template('my/status.html', user=g.user, subreddits = get_subreddits(), data=data)


@mod.route('/alter-password/', methods=['GET', 'POST'])
@requires_login
def alter_password():
    """
    修改密码
    """

    form = AlterPasswordForm(request.form)
    if form.validate_on_submit():
        if check_password_hash(g.user.password, form.oldPassword.data):
            g.user.password = generate_password_hash(form.newPassword.data)
            db.session.commit()
            flash(u'密码已更新！', 'success')
        else:
            flash(u'密码错误!', 'danger')
    return render_template('my/alter-password.html', form=form, user=g.user, subreddits = get_subreddits())



@mod.route('/reset_password/', methods=['post'])
@requires_login
def reset_password():
    """
    admin，用来重置某个账号的密码为123456
    """
    username = request.form.get('username', default=None)
    if username and g.user.is_admin():
        user = User.query.filter_by(username=username).first()
        if user:
            user.password = generate_password_hash('123456')
            db.session.commit()
            return jsonify(err=0, msg="success")
        else:
            return jsonify(err=2, msg="user not found!")
    return jsonify(err=1, msg="Forbidden!")