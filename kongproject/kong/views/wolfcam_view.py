# -*- coding: utf-8 -*-

from flask import (Blueprint, request, render_template, flash, g,
        session, redirect, url_for, jsonify, abort, send_from_directory)
from werkzeug import check_password_hash, generate_password_hash

from kong import db, app
from kong.models.owm_model import Owm
from kong.users.models import User
from kong.threads.models import Thread, Comment
from kong.users.decorators import requires_login
from kong.apis.openlog import OpenLog
from kong.frontends.views import get_subreddits
from kong import utils
from kong.forms import AlterPasswordForm
import base64
import random
import json
import uuid
import os

mod = Blueprint('wolfcam', __name__, url_prefix='/wolfcam')

def allowed_file(fn):
    return '.mp3' in fn

def secure_filename(fn):
    return 'ddd.mp3'

@mod.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return 'ok'
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''


@mod.route('/download/', methods=['GET', 'POST'])
def get_sound():
    uploads = app.config['UPLOAD_FOLDER']
    return send_from_directory(directory=uploads, filename='ddd.mp3')
    