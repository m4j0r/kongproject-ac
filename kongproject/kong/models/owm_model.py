# -*- coding: utf-8 -*-
from kong import db
from kong import utils
from kong import media
import datetime

class Owm(db.Model):
    __tablename__ = 'owm'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    state = db.Column(db.Integer, default=0)
    pay_end_day = db.Column(db.Integer, default=0)
    admin_note = db.Column(db.String(200), default=None)
    sync_time = db.Column(db.Integer, default=0)
    sync_data = db.Column(db.String(100000), default=None)
    token = db.Column(db.String(200), default=None)
    