# -*- coding: utf-8 -*-
from kong import db
from kong import utils
from kong import media
import datetime

class IrreaderTarget(db.Model):
    __tablename__ = 'irreader_target'
    id = db.Column(db.Integer, primary_key=True)
    sid = db.Column(db.String(64))
    name = db.Column(db.String(128))
    description = db.Column(db.String(256))
    source_url = db.Column(db.String(512))
    icon = db.Column(db.String(512))
    type = db.Column(db.Integer)
    category = db.Column(db.String(128))
    keywords = db.Column(db.String(128))
    category = db.Column(db.String(128))
    searchtext = db.Column(db.String(512))
    author = db.Column(db.String(64))
    charset = db.Column(db.String(16))
    min_len = db.Column(db.Integer)
    version = db.Column(db.Integer)
    sub_num = db.Column(db.Integer)
    grade = db.Column(db.Integer)
    mktst = db.Column(db.Integer)
    csser = db.Column(db.String(128))
    config = db.Column(db.String(512))
    add_time = db.Column(db.Integer)
    admin_state = db.Column(db.Integer)
    admin_note = db.Column(db.String(128))
    

    def to_dict(self):
        tmp_dict = self.__dict__.copy()
        del tmp_dict['_sa_instance_state'] #删除此无关的field
        return tmp_dict