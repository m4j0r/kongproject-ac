# -*- coding: utf-8 -*-
from flask import (Blueprint, request, send_file, render_template, flash, g, session,redirect, url_for, abort)
from kong.security.gen_yzm import gen_one
import io

mod = Blueprint('security', __name__, url_prefix='/security')

@mod.route('/secode')
def secode():
    '''
    注意，因为session是cookie based，所以这种方法可能存在重放的问题。
    可以加入时间戳来简单防御。暂时大部分人能防住了就行了。
    '''
    img, chs = gen_one()
    filee = io.BytesIO()
    img.save(filee, 'JPEG')
    filee.seek(0)
    session['secode'] = chs
    return send_file(filee, 'image/jpeg', cache_timeout=0)
