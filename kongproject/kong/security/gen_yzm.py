#!python
# -*- coding: UTF-8 -*-
# (C) 2018 netqon.com all rights reserved.

import random
import string
import sys
import math
import uuid
import os

from PIL import Image,ImageDraw,ImageFont,ImageFilter

_SIZE = (200, 64)
_FONT_PATHES = [
    'Arial.ttf'
    # '/Library/Fonts/Arial.ttf',
    # '/Library/Fonts/Arial Bold.ttf',
    # '/Library/Fonts/Arial Narrow Bold.ttf',
    # '/Library/Fonts/Arial Rounded Bold.ttf',
    # '/Library/Fonts/Bodoni 72 Smallcaps Book.ttf',
    # '/Library/Fonts/Comic Sans MS.ttf',
    # '/Library/Fonts/Georgia Bold.ttf',
    # '/Library/Fonts/Bradley Hand Bold.ttf'
    ]

_FONTS = []
_FONT_SIZE = _SIZE[1]
_NUM = 5

def random_color(start, end):
    return (random.randint(start, end),random.randint(start, end),random.randint(start, end))

def gen_char_paste(ch, color, rot, font, size, white=False):
    im_width = _FONT_SIZE
    im = Image.new('RGBA', (im_width, im_width), (0,0,0,0))
    draw = ImageDraw.Draw(im)
    font_width, font_height = font.getsize(ch)
    if white:
        #在下方写一个白色的阴影
        draw.text(((im_width-font_width)/2, (im_width-font_height)/2 + im_width/10), ch, fill=random_color(0x90, 0xA0), font=font)

    draw.text(((im_width-font_width)/2, (im_width-font_height)/2),ch, fill=color, font=font)
    im = im.rotate(rot)
    im = im.resize((size, size), Image.ANTIALIAS)
    return im

def get_bg_char():
    cand = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    c = random.choice(cand)
    im =  gen_char_paste(c, random_color(0x65, 0x75), random.randint(0, 720), random.choice(_FONTS), int(random.uniform(_SIZE[1]/8, _SIZE[1]/3)))
    return im

def load_fonts():
    for font_path in _FONT_PATHES:
        print(font_path)
        font_path = os.path.join(os.path.dirname(__file__), font_path)
        _FONTS.append(ImageFont.truetype(font_path, _FONT_SIZE))

load_fonts()

def gen_one(): 
    bg_color = random_color(0xEE, 0xFF)
    im = Image.new('RGB', _SIZE, bg_color)

    # 细斜线
    line_color = random_color(0x20, 0x40)
    draw = ImageDraw.Draw(im)
    line_y_offset = 0.25
    draw.line([(0,random.uniform(-_SIZE[1]*line_y_offset, _SIZE[1]*(1+line_y_offset))), 
        (_SIZE[0], random.uniform(-_SIZE[1]*line_y_offset, _SIZE[1]*(1+line_y_offset)))], fill=line_color, width=1)


    # 波浪曲线
    cos_y_offset = random.randint(0, _SIZE[1])
    cos_x_scale = random.uniform(0.15, 0.001)
    cos_y_scale = random.randint(0, _SIZE[1])
    cos_pos_list = []
    for x in range(0, _SIZE[0], 1):
        y = (math.sin(x*cos_x_scale))*cos_y_scale + cos_y_offset
        cos_pos_list.append((x,y))
    draw.line(cos_pos_list, fill=line_color, width=random.randint(3,5))


    # 背景杂字符
    ch_num = 30
    for i in range(ch_num):
        im_char = get_bg_char()
        im.paste(im_char, (random.randint(0, _SIZE[0]), random.randint(0, _SIZE[1])), im_char)

    # 验证码字符
    px = random.uniform(-_SIZE[0]/_NUM/6, _SIZE[0]/_NUM/4)

    chs = []
    pos = []
    for i in range(_NUM):
        cand = 'ABCDEFGHJKMNPQRSTUVWXY23456789' #没有z有2，没有1,i,l,0/O
        ch = random.choice(cand)
        chs.append(ch)
        # im_char = gen_char_paste(ch, random_color(0x20, 0x60), 0, _FONTS[0], _SIZE[1], white=True )
        rot = random.uniform(-35, 35)
        psize = int(random.uniform(_SIZE[1]/1.6, _SIZE[1])/1.2 )
        # psize = _SIZE[1]
        im_char = gen_char_paste(ch, random_color(0x20, 0x60), rot, random.choice(_FONTS), psize, white=True )
        px_center = int(0.5*(px + 0.5 * psize)) #第一个0.5时为了修正缩放
        px = int(px)
        py = int(random.uniform(-_SIZE[1]/10, _SIZE[1]/2.7))
        py_center = int(0.5*(py + 0.5 * psize)) #第一个0.5时为了修正缩放
        pos.append(px_center)
        pos.append(py_center)
        im.paste(im_char, (px, py), im_char)

        px += _SIZE[0]/(_NUM+1) + random.uniform(-_SIZE[0]/_NUM/5, _SIZE[0]/_NUM/5)

    return im, ''.join(chs)

    
if __name__ == "__main__":

    gen_one()
    
 
