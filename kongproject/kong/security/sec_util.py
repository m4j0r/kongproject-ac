# -*- coding: utf-8 -*-
from flask import (request, session, g)
from kong.users import constants as USER

def secode_validate():
    if g.user and g.user.role == 2:
        #admin不需要验证码
        return True
    real_code = session['secode']
    test_code = request.form['secode']
    print('secode cmp %s %s' % (real_code, test_code))
    if real_code and test_code and real_code.upper() == test_code.upper():
        return True
    return False

