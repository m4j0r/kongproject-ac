# -*- coding: utf-8 -*-

# For simplicity, these values are shared among both threads and comments.
MAX_TITLE = 300
MAX_BODY = 3000
MAX_LINK = 250

MAX_COMMENTS = 500
MAX_DEPTH = 10

# thread & comment status
DEAD = 0
ALIVE = 1
LOCK = 2

STATUS = {
    DEAD: 'dead',
    ALIVE: 'alive',
    LOCK: 'lock',
}

TYPE_ARTICLE = 0
TYPE_LINK = 1
TYPE_IMAGE = 2