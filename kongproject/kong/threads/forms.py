# -*- coding: utf-8 -*-
"""
"""
from kong.threads import constants as THREAD
from flask_wtf import Form
from wtforms import TextField, TextAreaField, BooleanField, IntegerField
from wtforms.validators import Required, URL, Length, Optional

class SubmitForm(Form):
    title = TextField('Title', [Required()])
    text = TextAreaField('Content', [Required()]) # [Length(min=5, max=THREAD.MAX_BODY)]
    link = TextField('Ref Link', [Optional(), URL(require_tld=True,
        message="That is not a valid link url!")])
    use_markdown = BooleanField('As Markdown')
    secode = TextField('Captcha', [Optional()]) #不做判断的，因为只有admin能发帖
    order = IntegerField('Order', [Optional()], default=0)
    path = TextField('Path', [Required()])