# -*- coding: utf-8 -*-
"""
"""
from kong.threads import constants as THREAD
from flask_wtf import Form
from wtforms import TextField, TextAreaField
from wtforms.validators import Required, URL, Length, Optional

class SubmitForm(Form):
    name = TextField(u'Project Name!', [Required()])
    desc = TextAreaField(u'Project Desc!', [Optional()])
    icon = TextField(u'Icon', [Optional()])
    intro = TextAreaField(u'Introduction', [Optional()])
    submit_note = TextAreaField(u'Submit note', [Optional()])
    csstext = TextAreaField(u'CSS text', [Optional()])
