# -*- coding: utf-8 -*-
"""
"""
from flask import (Blueprint, request, render_template, flash, g,
                   session, redirect, url_for, abort)

from kong.frontends.views import get_subreddits, process_thread_paginator
from kong.subreddits.forms import SubmitForm
from kong.threads.forms import SubmitForm as ThreadForm
from kong.subreddits.models import Subreddit
from kong.threads.models import Thread
from kong.users.models import User
from kong import db
from kong import utils
from kong.security.sec_util import secode_validate
from kong.media import get_img_from_mkd

mod = Blueprint('subreddits', __name__, url_prefix='/p')

#######################
### Subreddit Views ###
#######################


@mod.before_request
def before_request():
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])


def meets_subreddit_criterea(subreddit):
    return True


@mod.route('/subreddits/submit/', methods=['GET', 'POST'])
def submit():
    """
    """
    if g.user is None:
        flash('You must be logged in to submit subreddits!', 'danger')
        return redirect(url_for('frontends.login', next=request.path))

    form = SubmitForm(request.form)

    if not g.user.is_admin():
        flash(u'我没有权限创建project!', 'danger')
        return render_template('subreddits/submit.html', form=form, user=g.user,
                               subreddits=get_subreddits())

    user_id = g.user.id

    if form.validate_on_submit():
        name = form.name.data.strip()
        desc = form.desc.data.strip()

        subreddit = Subreddit.query.filter_by(name=name).first()
        if subreddit:
            flash('project already exists!', 'danger')
            return render_template('subreddits/submit.html', form=form, user=g.user,
                                   subreddits=get_subreddits())
        new_subreddit = Subreddit(name=name, desc=desc, admin_id=user_id)
        
        icon = form.icon.data.strip()
        if len(icon) > 0:
            new_subreddit.icon = icon
        
        new_subreddit.intro = form.intro.data
        new_subreddit.submit_note = form.submit_note.data
        new_subreddit.csstext = form.csstext.data

        if not meets_subreddit_criterea(subreddit):
            return render_template('subreddits/submit.html', form=form, user=g.user,
                                   subreddits=get_subreddits())

        db.session.add(new_subreddit)
        db.session.commit()

        flash('Thanks for starting a project! Begin adding issues to your project\
                by clicking the red button to the right.', 'success')
        return redirect(url_for('subreddits.permalink', subreddit_name=new_subreddit.name))
    return render_template('subreddits/submit.html', form=form, user=g.user,
                           subreddits=get_subreddits())


@mod.route('/subreddits/edit/<subreddit_name>/', methods=['GET', 'POST'])
def edit(subreddit_name):
    """
    """
    if g.user is None:
        flash('You must be logged in to edit subreddits!', 'danger')
        return redirect(url_for('frontends.login', next=request.path))

    subreddit = Subreddit.query.filter_by(name=subreddit_name).first()

    if not subreddit:
        abort(404)
    
    form = SubmitForm(request.form)
    if not form.name.data:
        form.name.data = subreddit.name
        form.desc.data = subreddit.desc
        form.icon.data = subreddit.icon
        form.intro.data = subreddit.intro
        form.submit_note.data = subreddit.submit_note
        form.csstext.data = subreddit.csstext

    if not g.user.is_admin():
        flash(u'我没有权限编辑Project!', 'danger')
        return render_template('subreddits/submit.html', form=form, user=g.user,
                               subreddits=get_subreddits())

    user_id = g.user.id

    if form.validate_on_submit():
        subreddit.name = form.name.data.strip()
        subreddit.desc = form.desc.data.strip()
        subreddit.icon = form.icon.data.strip()
        subreddit.intro = form.intro.data.strip()
        subreddit.submit_note = form.submit_note.data.strip()
        subreddit.csstext = form.csstext.data.strip()
        db.session.commit()

        flash('Project edit success!', 'success')
        return redirect(url_for('subreddits.permalink', subreddit_name=subreddit.name))
    return render_template('subreddits/submit.html', form=form, user=g.user,
                           subreddits=get_subreddits())

@mod.route('/delete/', methods=['GET', 'POST'])
def delete():
    """
    """
    pass


@mod.route('/subreddits/view_all/', methods=['GET'])
def view_all():
    """
    """
    return render_template('subreddits/all.html', user=g.user,
                           subreddits=Subreddit.query.all())


@mod.route('/<subreddit_name>/', methods=['GET'])
def permalink(subreddit_name=""):
    """
    """
    subreddit = Subreddit.query.filter_by(name=subreddit_name).first()
    if not subreddit:
        abort(404)

    trending = True if request.args.get('trending') else False
    thread_paginator = process_thread_paginator(
        trending=trending, subreddit=subreddit)
    subreddits = get_subreddits()

    return render_template('home.html', user=g.user, thread_paginator=thread_paginator,
                           subreddits=subreddits, cur_subreddit=subreddit)


@mod.route('/edit/<subreddit_name>/<thread_id>/', methods=['GET', 'POST'])
def thread_edit(subreddit_name=None, thread_id=None):
    """
    """
    if g.user is None:
        flash('You must be logged in to edit issue!', 'danger')
        return redirect(url_for('frontends.login', next=request.path))

    form = ThreadForm(request.form)
    thread = Thread.query.filter_by(id=thread_id).first()
    if thread == None or thread.user_id != g.user.id:
        abort(403)


    if len(form.title.data) <= 0:
        form.title.data = thread.title
        form.text.data = thread.text
        form.use_markdown.data = thread.use_markdown
        form.link.data = thread.link
        form.order.data = thread.order
        form.path.data = thread.path

    if form.validate_on_submit():
        if secode_validate():
            thread.title = form.title.data
            thread.link = form.link.data
            thread.text = form.text.data
            thread.use_markdown = form.use_markdown.data
            thread.order = form.order.data
            thread.path = form.path.data
            
            thumbnail = get_img_from_mkd(thread.text)
            if thumbnail and len(thumbnail) > 0:
                thread.thumbnail = thumbnail

            db.session.commit()

            flash(u'编辑成功', 'success')
            return redirect(url_for('threads.thread_permalink', subreddit_name=subreddit_name, thread_id=thread.id, title=thread.path))
        else:
            flash(u'验证码错误！', 'danger')
    
    return render_template('threads/submit.html', form=form, user=g.user,
                           subreddits=get_subreddits())
