# -*- coding: utf-8 -*-
"""
Written by:
Lucas Ou -- http://lucasou.com
"""
from flask import Flask, render_template, url_for

from flask_sqlalchemy import SQLAlchemy

from werkzeug.routing import BaseConverter

app = Flask(__name__, static_url_path='/static')
app.config.from_object('config')

db = SQLAlchemy(app)
from flaskext.markdown import Markdown

Markdown(app)

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]

app.url_map.converters['regex'] = RegexConverter

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def not_found(error):
    return render_template('500.html'), 500

from kong.users.views import mod as users_module
app.register_blueprint(users_module)

from kong.threads.views import mod as threads_module
app.register_blueprint(threads_module)

from kong.frontends.views import mod as frontends_module
app.register_blueprint(frontends_module)

from kong.apis.views import mod as apis_module
app.register_blueprint(apis_module)

from kong.subreddits.views import mod as subreddits_module
app.register_blueprint(subreddits_module)

from kong.security.captcha import mod as security_module
app.register_blueprint(security_module)

from kong.views.owm_view import mod as owm_module
app.register_blueprint(owm_module)

from kong.views.my_view import mod as my_module
app.register_blueprint(my_module)

from kong.views.irreader_view import mod as irreader_module
app.register_blueprint(irreader_module)

from kong.views.wolfcam_view import mod as wolfcam_module
app.register_blueprint(wolfcam_module)

def custom_render(template, *args, **kwargs):
    """
    custom template rendering including some kong vars
    """
    return render_template(template, *args, **kwargs)

app.debug = app.config['DEBUG']

if __name__ == '__main__':
    print('We are running flask via main()')
    app.run()
