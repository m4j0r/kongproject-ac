# -*- coding: utf-8 -*-
"""
Logic handling user specific input forms such as logins and registration.
"""
from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, BooleanField
from flask_wtf.recaptcha import RecaptchaField
from wtforms.validators import Required, EqualTo, Email


class AlterPasswordForm(FlaskForm):
    oldPassword = PasswordField(u'旧密码', [Required()])
    newPassword = PasswordField(u'新密码', [Required()])