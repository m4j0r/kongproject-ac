var g_query = null
var g_channel = "latest"
var g_page = 0
var g_first = true
var g_last = true

document.addEventListener('DOMContentLoaded', function() {
    console.log('hello')
    locale_init()
    init_channels()
    init_search()
    make_request()
})

var g_cate_map = {
    podcast: '播客',
    internet: '互联网',
    technology: '科技',
    programming: '编程技术',
    infosec: '信息安全',
    startup: '创业和产品',
    design: '设计',
    media: '电影图书',
    science: '学术科研',
    hardware: '消费电子',
    entertainment: '娱乐',
    second: '二次元',
    news: '时事新闻',
    people: '人物和组织',
    management: '财经管理',
    game: '游戏',
    nsfw: 'NSFW',
    culture: '文化生活',
    travel: '旅行户外摄影',
    community: '社区',
    trade: '购物交易',
    download: '资源下载',
    blog: '独立博客',
    tool: '工具',
    other: '其他'
}

function get_query_param(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}
var g_use_cn = true
function locale_init() {
    g_use_cn = get_query_param('lan') != 'en'
    var use_cn = g_use_cn
    var nodes = document.all
    for (var i = 0; i < nodes.length; i++) {
        var o = nodes[i]
        if (use_cn && o.hasAttribute('cn')) {
            o.innerHTML = o.getAttribute('cn')
        }
        if (use_cn && o.hasAttribute('title-cn')) {
            o.setAttribute('title', o.getAttribute('title-cn'))
        }
        if (use_cn && o.hasAttribute('placeholder-cn')) {
            o.setAttribute('placeholder', o.getAttribute('placeholder-cn'))
        }
        if (!use_cn && o.hasAttribute('en')) {
            o.innerHTML = o.getAttribute('en')
        }
        if (!use_cn && o.hasAttribute('title-en')) {
            o.setAttribute('title', o.getAttribute('title-en'))
        }
        if (!use_cn && o.hasAttribute('placeholder-en')) {
            o.setAttribute('placeholder', o.getAttribute('placeholder-en'))
        }
    }
}

function make_request() {

    var page_size = 18

    if (window.innerWidth > 1200) {
        page_size = 20
    }

    var data = {p:g_page, s: page_size, r: 'hot'}
    if (g_query) {
        data.q = g_query
    } else {
        if (g_channel == "featured") {
            data.m = 1
        } else {
            data.c = g_channel
        }
    }
    if (g_channel == 'latest') {
        data.r = 'new'
    }

    $.post('/irreader/get_sources/', data, on_request_result)
}

function  lg(cn, en) {
    return g_use_cn ? cn:en
}

function on_request_result(data) {
    $('#content').empty()
    if (data.targets) {
        $('#prev_page').attr('works', 'true')
        $('#next_page').attr('works', 'true')
        $('#message').text(lg(`找到${data.targets.length}个源`, `Found ${data.targets.length} Sources `))
        data.targets.forEach(add_target_item_element)
    }
}

function  on_click_page_navi(direction) {
    if (direction == 'next') {
        g_page += 1
        make_request()
    }

    $('#page_number').text(g_page)

    if (direction == 'prev' && g_page > 0) {
        g_page -= 1
        make_request()
    }
    
}

function add_target_item_element(target) {
    var new_element = $('#target_item_template').clone()
    new_element.removeAttr('id')
    if (target.icon && target.icon.length > 0) {
        new_element.find('img').attr('src', target.icon)
    }
    if (target.category == 'podcast') {
        new_element.find('.is-podcast').show()
    }
    new_element.find('.target-item-name').text(target.name)
    var cate_text = g_cate_map[target.category]
    if (cate_text) {
        new_element.find('.target-item-author').text(cate_text)
    }
    new_element.find('.target-item-description').text(target.description)
    new_element.find('.target-item-subnum').text(target.sub_num)
    new_element.appendTo('#content')
    //要用encodeURIComponent，才能把&等符号也转义
    new_element.attr('href', `likefeed://add-remote?name=${encodeURIComponent(target.name)}&source=${encodeURIComponent(target.source_url)}&type=${target.type}&csser=${encodeURIComponent(target.csser)}&min_len=${target.min_len}&desc=${encodeURIComponent(target.description)}&icon=${encodeURIComponent(target.icon)}&charset=${encodeURIComponent(target.charset)}&config=${encodeURIComponent(target.config)}`)
    new_element.attr('target', '_blank')
    new_element.click(function () {
        $.post('/irreader/sub_num_plus/', {id: target.id}, function (data) {
            console.log(data)
        })
    })
    new_element.find('.dynamic').hide()

    try{
        target.my_config = JSON.parse(target.config)
        if (target.my_config.dynamic == 'true') {
            new_element.find('.dynamic').show()
        }
    } catch(err) {

    }


}

function dev() {
    for(var i = 0; i < 15; i++) {
        var target_element = $('#target_item_template').clone()
        target_element.removeAttr('id')
        target_element.appendTo('#content')
    }
}


function  init_channels() {
    $('#side li').click(function (event) {
        var selected_channel = $(event.target).attr('cat')

        //clear select
        $('#side li').attr('sel', 'false')
        $(`#side li[cat="${selected_channel}"]`).attr('sel', 'true')
        console.log(selected_channel)

        g_query = null
        g_channel = selected_channel
        g_page = 0

        make_request()
    })
    
}

function  init_search() {
    $('#search_input').get(0).addEventListener("keyup", function (event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            on_serarch_enter()
        }
    })
}

function on_serarch_enter() {
    $('#side li').attr('sel', 'false')
    g_query = $('#search_input').val()
    g_page = 0
    g_channel = null
    make_request()
    $('#search_input').blur()
}