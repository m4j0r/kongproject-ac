
console.log('base.js')

document.addEventListener('DOMContentLoaded', function () {
    var url = new URL(window.location.href);
    var rank_type = url.searchParams.get("r");
    if (!rank_type) { rank_type = 'all'; }

    $(`.header-tab[r="${rank_type}"]`).attr('pressed', 'true');

    console.log('init')

    $('#secode').click(function () {
        $('#secode').attr('src', $('#secode').attr("src") + "1")
    })

})

function toggle_pin(thread_id) {
    $.post("/apis/threads/toggle_pin/", { thread_id: thread_id }, function (data) {
        console.log(data)
        location.reload()
    });
}

function delete_thread(thread_id) {
    var ret = confirm('delete?')
    if (ret) {
        $.post("/apis/threads/set_status/", { thread_id: thread_id, status: 0 }, function (data) {
            console.log(data)
            location.reload()
        });
    }
}