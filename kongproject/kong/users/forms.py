# -*- coding: utf-8 -*-
"""
Logic handling user specific input forms such as logins and registration.
"""
from flask_wtf import FlaskForm
from wtforms import TextField, PasswordField, BooleanField
from flask_wtf.recaptcha import RecaptchaField
from wtforms.validators import Required, EqualTo, Email


class LoginForm(FlaskForm):
    username = TextField(u'用户名', [Required()])
    # email = TextField(u'Email地址', [Required(), Email()])
    password = PasswordField(u'密码', [Required()])


class RegisterForm(FlaskForm):
    username = TextField(u'用户名', [Required()])
    email = TextField(u'Email地址', [Required(), Email()])
    password = PasswordField(u'密码', [Required()])
    confirm = PasswordField(u'密码确认', [
        Required(),
        EqualTo('password', message=u'两次输入的密码不一致，请检查')
    ])
    accept_tos = BooleanField(u'我接受', [Required()])
    secode = TextField(u'验证码', [Required()])
