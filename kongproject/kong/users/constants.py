# -*- coding: utf-8 -*-

MAX_THREADS_PER_DAY = 100
MAX_COMMENTS_PER_DAY = 500
MAX_VOTES_PER_DAY = 2000

MAX_USERNAME = 80
MAX_EMAIL = 200
MAX_PASSW = 200

# User status
STATUS_DEAD = 0
STATUS_ALIVE = 1

STATUS = {
    STATUS_DEAD: 'dead',
    STATUS_ALIVE: 'alive',
}

# User role
ROLE_ADMIN = 2
ROLE_STAFF = 1
ROLE_USER = 0

ROLE = {
    ROLE_ADMIN: 'admin',
    ROLE_STAFF: 'staff',
    ROLE_USER: 'user',
}
