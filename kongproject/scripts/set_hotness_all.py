#!/usr/bin/env python2.7
"""
"""
import os
import sys
sys.path.insert(0, '/home/lucas/www/reddit.lucasou.com/reddit-env/kong')
import readline
from pprint import pprint

from flask import *
from kong import *

from kong.users.models import *
from kong.threads.models import *
from kong.subreddits.models import *
from kong.threads.models import thread_upvotes, comment_upvotes

threads = Thread.query.all()
for thread in threads:
    thread.set_hotness()

import time
print('Hotness values have been computed for all threads without error on', \
    time.strftime("%H:%M:%S"), \
    time.strftime("%d/%m/%Y"))
