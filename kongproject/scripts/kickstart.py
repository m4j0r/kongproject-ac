#!/usr/bin/env python2.7
"""
This script instantiates critical components of our webapps.
We need at least one home subreddit to get things going.
We also need a first user to admin our first subreddit.
"""
import os
import sys
import readline
from pprint import pprint

from flask import *
from werkzeug import check_password_hash, generate_password_hash

sys.path.insert(0, '/Users/ch3n/Projects/kongproject/kong/')
from kong import *
from kong.users.models import *
from kong.threads.models import *
from kong.subreddits.models import *

db.drop_all()
db.create_all()
print('create tables')
first_user = User(username='root', email='bingfeng.ch3n@gmail.com', \
        password=generate_password_hash('123456'))

db.session.add(first_user)
db.session.commit()

first_subreddit = Subreddit(name='frontpage', desc='Welcome to Reddit! Here is our homepage.',
        admin_id=first_user.id)

db.session.add(first_subreddit)
db.session.commit()
