#!/usr/bin/env python2.7
"""
/shell.py will allow you to get a console and enter commands within your flask environment.
"""
import os
import sys
import readline
from pprint import pprint

from flask import *

sys.path.insert(0, '/home/lucas/www/reddit.lucasou.com/reddit-env/kong')
from kong import *
from kong.users.models import *
from kong.threads.models import *
from kong.subreddits.models import *
from kong.threads.models import thread_upvotes, comment_upvotes

os.environ['PYTHONINSPECT'] = 'True'
