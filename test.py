import re
   
def get_img_from_mkd(mkd):
    ret = re.search("^!\[.*\]\((.*)\)", mkd, re.M)
    if ret:
        return ret.group(1)
    else:
        return None


print(get_img_from_mkd("sd\n![sdfsdf](http://baidu.com/favicon.ico)sdfsdf"))
